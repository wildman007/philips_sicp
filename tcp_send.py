import socket
import sys

def send_data (IP, PORT, msg, debug=False, receive=True):
	inmsg=""
	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# Connect the socket to the port where the server is listening
	server_address = (IP, PORT)
	if(debug):
		print('connecting to {} port {}'.format(*server_address))
	sock.connect(server_address)
	
	try:
		# Send data
		message = msg
		if(debug):
			print('sending {!r}'.format(message))
		sock.sendall(message)

		if(receive):
			# Look for the response
			amount_received = 0
			amount_expected = len(message)

			data = sock.recv(1024)
			if not data:
				inmsg=""
			inmsg=data
			if(debug):
				print('received {!r}'.format(inmsg))

	finally:
		if(debug):
			print('closing socket')
		sock.close()
	return inmsg
