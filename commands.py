class Command:
	data=b''
	def __init__(self):
		pass
	
	def bytes():
		return data

	def parse_answer(self,answer):
		return answer

	def framed_data(self):
		size=len(self.data)+2
		fdata=size.to_bytes(1,'big')+self.data
		#check sum
		s=0
		for i in fdata:
			s^=i
		return fdata+s.to_bytes(1,'big')
	
	def hasResponse(self):
		return True

class PWR(Command):
	def __init__(self):
		self.data=b'\x01\x00\x19'

	def parse_answer(self,answer):
		if(answer[4]==1):
			return "OFF"
		else:
			return "ON"

class ON(Command):
	def __init__(self):
		self.data=b'\x01\x00\x18\x02'

	def parse_answer(self,answer):
		return "done"

	def hasResponse(self):
		return False

class OFF(Command):
	def __init__(self):
		self.data=b'\x01\x00\x18\x01'

	def parse_answer(self,answer):
		return "done"

	def hasResponse(self):
		return False
		
class TEMP(Command):
	def __init__(self):
		self.data=b'\x01\x00\x2F'
	
	def parse_answer(self,answer):
		return str(int(answer[4]))+"°C"

class SCHED(Command):
	def __init__(self):
		self.data=b'\x01\x00\x5b\x01'
	
	def parse_answer(self,answer):
		return answer

