import sys
import commands
from tcp_send import send_data

debug=False

def process_command(cmd):
	if(cmd=="PWR"):
		return commands.PWR()
	elif(cmd=="ON"):
		return commands.ON()
	elif(cmd=="OFF"):
		return commands.OFF()
	# if(cmd=="SOURCE"):
	# 	data=b'\x01\x00\xAD'
	elif(cmd=="TEMP"):
		return commands.TEMP()
	elif(cmd=="SCHED"):
		return commands.SCHED()
	else:
		return None

def print_hex(msg):
	for i in msg:
		print(hex(i), end=" ")
	print("")


IP = "10.100.14.249"
PORT = 5000
CMD=""
if(len(sys.argv)==4):
	if(sys.argv[3]=="d"):
		debug=True

if(debug):
	print("Philips SICP")
	print("---------------------")
# print(sys.argv)
if(len(sys.argv)>=3):
	IP=sys.argv[1]
	CMD=sys.argv[2]
	if(debug):
		print("target IP: %s" % IP)
		print("target port: %s" % PORT)
		print("CMD: %s" % CMD)

	cmd=process_command(CMD)
	if(cmd!=None):
		data=cmd.framed_data()
		if(debug):
			print("Sending: ")
			print_hex(data)
			print("")
		out=send_data(IP, PORT, data, receive=cmd.hasResponse())
		if(out!=None):
			if(debug):
				print("Received:")
				print_hex(out)
			print(cmd.parse_answer(out)) 

	else:
		print("Invalid command")

else:
	print("Usage: philips_sicp.py ip_address command")
	print("")
	print("       ip_address - Philips display IP address")
	print("          command - one of the following:")
	print("                         PWR - get power status")
	print("                         ON - turn on display")
	print("                         OFF - turn off display")
	# print("                         SOURCE - get current source")
	print("                         TEMP - get current temp")
	print("                d - enables debug")
	
