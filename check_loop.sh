#!/bin/bash

while :; do
	d=$(date)
	echo $d
	ips="10.100.14.249 10.100.14.189 10.100.14.168"

	for ip in $ips; do
		pw=$(python3 philips_sicp.py $ip PWR)
		tm=$(python3 philips_sicp.py $ip TEMP)
		echo $d $ip $pw $tm >>log.txt
		if [ "$pw" == "OFF" ]; then
			echo "	trying to power ON $ip" >>log.txt
			python3 philips_sicp.py $ip ON
		fi
	done
	sleep 120
done
